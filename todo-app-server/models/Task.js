const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ItemSchema = new Schema({
	name: String,
	status: String
});

module.exports = mongoose.model('Task', ItemSchema);
