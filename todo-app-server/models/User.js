const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = mongoose.Schema({
    name: String,
    email: String,
	username: {
		type: String,
		unique: true
	},
	password: String
});

module.exports = mongoose.model('User', UserSchema);
