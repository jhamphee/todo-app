const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/User');

const router = express.Router();

router.post('/', (req, res) => {
	User.findOne({username: req.body.username})
	.then(data => {
		if(!data) {	
			let newUser = new User(req.body);
			newUser.password = bcrypt.hashSync(req.body.password, 10);
			newUser.save()
			.then(data => {
				res.send(data);
			});
		} else {
			res.send("username exists");
		}
	})
})

// router.post('/login', (req, res) => {
// 	User.findOne({ username: req.body.username })
// 	.then(data => {
// 		if(data) {
// 			const match = bcrypt.compareSync(req.body.password, data.password)
// 			if(match) {
// 				data.password = undefined;
// 				res.send(data);
// 			} else {
// 				res.send("wrong password");
// 			}
// 		} else {
// 			res.send("wrong username");
// 		}
// 	})
// })


module.exports = router;