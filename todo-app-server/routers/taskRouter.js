const express = require('express');
const path = require('path');
const fs = require('fs');
const Task = require('../models/Task');

const router = express.Router();


router.get('/', (req, res) => {
	Task.find({})
	.then(data => {
		res.send(data)
	});	
})

router.post('/', (req, res) => {
    let newTask = new Task (req.body);
    newTask.save()
    .then(data => {
        res.send(data);
    });
})


router.delete('/:id', (req, res) => {
	Task.findOneAndDelete({_id: req.params.id}, { useFindAndModify: false }) 
	.then(data => {
		res.send(data);
	});
})

router.patch('/:id', (req, res) => {
    Task.updateOne({_id: req.params.id}, req.body, { new: true, useFindAndModify: false })
    .then(data => {
        res.send(data);
    });
})

// router.patch('/:id', (req, res) => {
// 	let Task = req.body;
// 	Task.findOneAndUpdate({_id: req.params.id}, req.body, { new: true, useFindAndModify: false })
// 	.then(data => {
// 		res.send(data);
// 	})
// })

module.exports = router;