const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const taskRouter = require('./routers/taskRouter');
const userRouter = require('./routers/userRouter');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/todo-app', { useNewUrlParser: true,  useUnifiedTopology: true });

mongoose.set('useCreateIndex', true);

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/users', userRouter);
app.use('/tasks', taskRouter);

app.listen(8080, () => console.log("running on port 8080"));