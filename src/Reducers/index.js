import { combineReducers } from 'redux';
// import user from './userReducer';

const axios = require('axios');

const initialState = {
    task: [],
    isLoggedIn : localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : false
}

const rootReducer = (state = initialState, action) => {

    switch(action.type) {
        case 'LOGIN':
            return {
				isLoggedIn: action.payload
			}
		case 'LOGOUT':
			return {
				isLoggedIn: false
			}
    }
    
    if (action.type === "DATA_BASE"){
        let temp = action.payload
        return { task : temp }
    }

    if (action.type === "ADD_TASK"){
        let temp = JSON.parse(JSON.stringify(state.task))
        temp.push(action.payload);
        axios.post('http://localhost:8080/tasks', action.payload)
        return {task : temp}
    }

    if (action.type === "RMV_BTN"){
        let temp = JSON.parse(JSON.stringify(state.task))
        let taskIndex = 0;
        temp.forEach((task, index) => {
            if (task._id === action.payload){
                taskIndex = index
            }
        })
        temp.splice(taskIndex, 1);
        return {task : temp}
    }

    if (action.type === "CHNG_STAT"){
        let temp = JSON.parse(JSON.stringify(state.task))
        let updatedStatus = temp.map(task => {
            if(task._id === action.payload){
                if(task.status === "pending"){
                    task.status = "done"
                } else {
                    task.status = "pending"
                }
            }
            return task
        })
        return { task : updatedStatus}
    }

}

export default rootReducer;