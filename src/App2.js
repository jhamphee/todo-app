import React from 'react';
import './App.css';
import AddNew from './Components/AddNew';
// import DoneTask from './Components/DoneTask';
// import PendingTask from './Components/PendingTask'; 
import { json } from 'body-parser';


const axios = require('axios');

class App extends React.Component{

  state = {
    tasks: []
  }

  
  componentDidMount(){
    axios.get('http://localhost:8080/tasks')
    .then(taskArray => {
      this.setState({
        tasks: taskArray.data
      });
    });
  }
  
taskPending = () => {
  let taskArray = this.state.tasks.map((a) => {
    if (a.status === "pending"){
      return <div className = "onPendingTask clearfix">
                  <span>{a.name}</span>
                  <button type="button" className="removeBtnPending" id={a._id} onClick={this.rmvTaskBtn}>
                    <img src = "https://image.flaticon.com/icons/svg/1632/1632602.svg" alt = "delete"/>
                  </button>
                  <input className="checkBox" type="checkbox" id={a._id} onChange={this.checkBoxHandler}/>
              </div>
      }
    });
    return taskArray
  }
    
taskDone = () => {
  let taskArray = this.state.tasks.map((a) => {
    if (a.status === "done"){
      return <div className = "thisIsDone clearfix">
              <span>{a.name}</span>
              <button type="button" id={a._id} className="removeBtnDone" onClick={this.rmvTaskBtn}>
                <img src = "https://image.flaticon.com/icons/svg/1632/1632602.svg" alt = "delete"/>
              </button>
              <input className="checkBox" type="checkbox" onChange={this.checkBoxHandler} id={a._id}/>
            </div>
  }
});
    return taskArray 
  }
  
// inputChangeHandler = (event) => {
//       this.setState({
//           newTask: event.target.value,
//         });
//       }
    
    addTaskBtn = (newObj) => {
      let temp = JSON.parse(JSON.stringify(this.state.tasks));
      temp.push(newObj);
      this.setState({
        tasks: temp
      });   
    }
    
//     checkBoxHandler = (event) => {
//       let temp = JSON.parse(JSON.stringify(this.state.tasks));
//       if (event.target.checked === true){
//         if(temp[event.target.id].status === "pending"){
//           temp[event.target.id].status = "done"
//         } else {
//           temp[event.target.id].status = "pending"
//         }
//     this.setState({
//       tasks: temp
//     });
//   }
// }

rmvTaskBtn = (id) => {
  axios.delete('http://localhost:8080/tasks/'+ id)
  .then(data => {
    if (data){
      let temp = JSON.parse(JSON.stringify(this.state.tasks));
      temp = temp.filter( t => t._id !== id);
      this.setState({
        tasks: temp
      })
    }
  })
}

checkBoxHandler = (id) => {
  axios.put('http://localhost:8080/tasks'+id, {status : "done"})
  .then(res => {
    let temp = JSON.parse(JSON.stringify(this.state.tasks));
    let updatedArray = temp.map( t => {
      if (t._id === id){
        t.status = "done";
      }
      return t;
    })
    this.setState({tasks: updatedArray});
  })
}

// pendingTaskCheck = () => {
//     let temp = JSON.parse(JSON.stringify(this.state.tasks));
//         temp.map(a,b) = () => {
//             if (a.status !== "pending"){
//                 return <span>{a.noPending}</span>
//               }
//          }
//       }
      
      
      render(){
        // console.log(this.checkBoxHandler());
        
      let taskArrPending = this.state.tasks.filter( task => {
        return task.status === "pending";
      });
          
      let taskArrDone = this.state.tasks.filter( task => {
          return task.status === "done";
      });

      let filteredTask = () => {
          if(taskArrPending.length <= 0){
              return <span>"No pending task."</span>
          } 
      }  
      return(
          <div className = "App">
          <header> 
            <h1>My To-Do List</h1>
          </header>
          <div className = "App-header">
              <div className = "newTaskDiv">
                <div className = "headerDiv">
                <span className="error">{this.state.errorMessage}</span>
                  <h3>New Task </h3>
                </div>
                <form>
                 <AddNew id = {this.state.tasks.id} tasks = {this.state.tasks} newtask = {this.state.newtask} errorMessage = {this.state.errorMessage} addTaskBtn = {this.addTaskBtn} inputChangeHandler = {this.inputChangeHandler}/>
                </form>
              </div>

              <div className = "pendingTaskDiv">
                <div className = "headerDiv">
                  <h3 className="clearfix">Pending Task</h3>
                </div>
                  {this.taskPending()}
                  {filteredTask()}
              </div>

              <div className = "doneTaskDiv">
                <h3 className="clearfix">Done Task</h3>
                {this.taskDone()}
              </div>
          </div>
      </div>

    )
  }

}


export default App;
