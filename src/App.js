import React from 'react';

import Navbar from './Components/Navbar';
import Home from './Components/Home';
import SignUp from './Components/SignUp';
import SignIn from './Components/SignIn';
import Profile from './Components/Profile';

import {connect} from 'react-redux';

// import {Provider} from 'react-redux';
// import {createStore} from 'redux';
// import rootReducer from './Reducers/index';

import {BrowserRouter as Router, Route} from 'react-router-dom';

import './App.css';

// const store = createStore(rootReducer)

const axios = require('axios')

const dataBaseHandler = (data) => {
   return {
     type: "DATA_BASE",
     payload: data
   }
  }

class App extends React.Component{

  componentDidMount(){
    axios.get('http://localhost:8080/tasks')
    .then(res => {
      let newData = res.data;
      this.props.addToState(newData)
    });
  }
  
  
  render(){
    return(
      <Router>
        <div className="App clearfix">

        <Navbar />

        <Route exact path="/">
            <Home />
        </Route>

        <Route path="/login">
            <SignIn />
        </Route>

        <Route path="/create">
            <SignUp />
        </Route>

        <Route path="/help">
            <Profile />
        </Route>

      </div>

      <footer>My Tasks 2020 © All Rights Reserved.</footer>
      </Router>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToState : (newData) => {dispatch(dataBaseHandler(newData))}

  }
}

export default connect(null, mapDispatchToProps)(App);