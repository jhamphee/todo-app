import React from 'react';

import {connect} from 'react-redux';

const addTaskBtnDispatch = (task) => {
    return {
        type : "ADD_TASK",
        payload: task
    }
}

const axios = require('axios')

class AddNew extends React.Component{

state = {
    input: "",
    errorMessage: ""
}

inputTask = (event) => {
    this.setState({
        input: event.target.value
    })
}

addTaskBtn = () => {
    if(this.state.input.trim() === ""){
        this.setState({
            errorMessage : "This field cannot be empty."
        })
    } else {
        let newTask = {
            id: "",
            name: this.state.input,
            status: "pending"
        }
        this.props.taskPending(newTask)
        this.setState({
            input : ""
        })

    } 
}

keyPress = (event) => {
    if(event.key === "Enter"){
        event.preventDefault();
        this.addTaskBtn()
    }
}

    render(){
        return(
            <div className="newTaskContainer">
                <input id="inputTask" type="text" onKeyPress = {this.keyPress} onChange = {this.inputTask} value = {this.state.input}/>
                <button id = "newTaskBtn" onClick={this.addTaskBtn} type="button">ADD TASK</button>
                <div className="error">{this.state.errorMessage}</div>
            </div>                  
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        taskPending : newTask => {dispatch(addTaskBtnDispatch(newTask))}
    }
}


export default connect(null, mapDispatchToProps)(AddNew);