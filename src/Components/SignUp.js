import React from 'react';

const axios = require('axios');



class SignUp extends React.Component{

    state = {
        name: "",
        email: "",
		username: "",
		password: "",
        cpassword: "",
        error: ""
	}

	submitHandler = (e) => {
        e.preventDefault();
        
        if(this.state.password !== this.state.cpassword){
            this.setState({
                error: "Password does not match."
            });
        } else {
            axios.post("http://localhost:8080/users",
            {
                name: this.state.name,
                email: this.state.email,
                username: this.state.username, 
                password: this.state.password
            })
		.then(res => {
            console.log(res.data);
            alert("Thank you for signing up! Login to start.")
		});
        }
	}


    render(){
        return(
            <div className = "signUpComponent clearfix">
                <form className = "formContainerSignUp">
                    <h2>Create Account</h2>
                    
                    <div className = "nameContainer">
                        <input type = "text" 
                               placeholder = "Full Name"
                               value = {this.state.name}
                               onChange = {e => this.setState({name: e.target.value})}              
                        >
                        </input>
                    </div>

                    <div className = "emailContainer">
                        <input type = "email" 
                               placeholder = "Email"
                               value = {this.state.email}
                               onChange = {e => this.setState({email: e.target.value})}
                        >
                        </input>
                    </div>

                    <div className = "userNameContainer">
                        <input type = "text" 
                               placeholder = "Username"
                               value = {this.state.username}
                               onChange = {e => this.setState({username: e.target.value})}
                        >
                        </input>
                    </div>

                    <div className = "passwordContainer">
                        <input type = "password"
                               placeholder = "Password"
                               value = {this.state.password}
                               onChange = {e => this.setState({password: e.target.value})}
                        >
                        </input>
                    </div>

                    <div className = "passwordContainer">
                        <input type = "password" 
                               placeholder = "Confirm Password"
                               value = {this.state.cpassword}
                               onChange = {e => this.setState({cpassword: e.target.value})}
                        >        
                        </input>
                    </div>

                    <div className = "error">{this.state.error}</div>

                    <div>
                        <button type = "button" className = "signUpBtnContainer" onClick = {this.submitHandler}>Sign me up!</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default SignUp;