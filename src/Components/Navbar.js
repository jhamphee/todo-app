import React from 'react';
import Navitem from './Navitem';

class Navbar extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            'NavItemActive':''
        }
    }
    activeitem=(x)=>
    {
        if(this.state.NavItemActive.length>0){
            document.getElementById(this.state.NavItemActive).classList.remove('active');
        }
        this.setState({'NavItemActive':x},()=>{
            document.getElementById(this.state.NavItemActive).classList.add('active');
        });
    };

    render(){
        return(
            <div className = "navBar clearfix">
                <Navitem item="HOME" tolink="/"  activec={this.activeitem}></Navitem>
                <Navitem item="SIGN IN" tolink="/login"  activec={this.activeitem}></Navitem>
                <Navitem item="GET STARTED" tolink="/create"  activec={this.activeitem}></Navitem>
                <Navitem item="GET HELP" tolink="/help"  activec={this.activeitem}></Navitem>
            </div>
        )
    }
}



export default Navbar;