import React from 'react';
import { connect } from 'react-redux';

import axios from 'axios';

class SignIn extends React.Component{

    state = {
        username: "",
        password: ""
    }

    loginBtnHandler = (e) => {
		e.preventDefault();

		axios.post("http://localhost:8080/users/login", {username:this.state.username, password: this.state.password})
		.then(res => {
			if(res.data) {
				localStorage.setItem("user", JSON.stringify(res.data));
				this.props.login(res.data);
				this.props.history.push('/');
			}
		});

	}



    render(){
        return(
            <div className = "signInComponent clearfix">

                <form className = "formContainerSignIn">
                    <h2>SIGN IN</h2>

                    <div>
                        <img src = "https://image.flaticon.com/icons/svg/1246/1246351.svg" id = "userImgContainer"/>
                    </div>

                    <div className = "inputContainer">
                        <input type = "text" 
                            placeholder = "Username"  
                            onChange = {e=>this.setState({username: e.target.value})}
                            value = {this.state.username}
                        />
                    </div>

                    <div className = "inputContainer">
                        <input type = "password" 
                            placeholder = "Password" 
                            onChange = {e=>this.setState({password: e.target.value})}
                            value = {this.state.password}
                        />
                    </div>

                    <div className = "btnContainer">
                        <button type = "button" 
                            onClick = {this.signInBtnClickHandler}
                            onClick = {this.loginBtnHandler}
                        >Login
                        </button>
                    </div>
                    <a href = "#">Forgot your password?</a>

                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
	return {
		login: user => dispatch({
			type: 'LOGIN',
			payload: user,
		})
	}
}


export default connect(null, mapDispatchToProps )(SignIn);