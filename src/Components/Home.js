import React from 'react';
import ReactTypingEffect from 'react-typing-effect';


class Home extends React.Component{
    render(){
        return(
            <div className = "homeContainer">

                <div>
                    <ReactTypingEffect className="typingeffect" text={['Write down what you need to do WHENEVER. WHEREVER.']} speed={100}/>
                </div>

            </div>
        )
    }
}

export default Home;