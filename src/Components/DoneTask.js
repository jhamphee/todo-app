import React from 'react';
import {connect} from 'react-redux';

const axios = require('axios');


const rmvTaskBtnHandler = (taskId) => {
    return {
        type : "RMV_BTN",
        payload : taskId    
    }
}

const changeBtnHandler = (taskId) => {
    return {
        type : "CHNG_STAT",
        payload : taskId    
    }
}

const DoneTask = (props) => {

    let taskBtnHandler = (event) => {
        if(event.target.className.includes("delete")){
            axios.delete('http://localhost:8080/tasks/'+event.target.id)
            props.rmvTask(event.target.id);      
        } else if(event.target.className.includes("change")){
            axios.patch('http://localhost:8080/tasks/'+event.target.id, {status: "pending"})
            props.chngeStatus(event.target.id);
        } else {
        }
    }

    let taskDoneDisplay = () => {
        let displayFilter = props.tasks.filter(task => {
            return (task.status === "done")
        })
        let displayMap = displayFilter.map(a => {
            return <div className = "taskListContainer clearfix" onClick = {taskBtnHandler}>
                    <div className = "taskContainer">{a.name}</div>
                    <div className = "taskBtnContainer"> 
                     <button id = {a._id} type = "button" className = "delete TodoBtnHandler">
                         <img src = "https://image.flaticon.com/icons/svg/1215/1215092.svg"/>
                     </button>
                     <button id = {a._id} type = "button" className = "change TodoBtnHandler">
                         <img src = "https://image.flaticon.com/icons/svg/1783/1783850.svg"/>
                     </button>
                    </div>
                   </div>
        })
        return displayMap
    }

    return(
        <div className = "taskDisplay">
            <h2>DONE</h2>
            {taskDoneDisplay()}
        </div>
    )
}

const mapStateToProps = (state) => {
    return {tasks : state.task}
}

const mapDispatchToProps = (dispatch) => {
    return {
        rmvTask : taskId => {dispatch(rmvTaskBtnHandler(taskId))},
        chngeStatus : taskId => {dispatch(changeBtnHandler(taskId))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DoneTask);