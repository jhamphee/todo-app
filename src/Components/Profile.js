import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import AddNew from './AddNew';
import PendingTask from './PendingTask';
import DoneTask from './DoneTask';

const Profile = (props) => {
        return(
            <div className = "profileContainer clearfix">
                <div className = "headerProfileContainer clearfix">
                    
                    <div className = "leftHeaderContainer">
                        <div className = "profilePhoto">
                            <img src = "https://image.flaticon.com/icons/svg/1864/1864609.svg"/>
                        </div>
                        <div className = "editProfile">
                            <button>edit</button>
                        </div> 
                    </div>

                    <div className = "rightProfileContainer">
                        <h2>Jhamila Padilla</h2>
                        <h4>Makati City, Philippines</h4>
                    </div>

                    <div>
                        {
                        this.props.isLoggedIn ?
                            <div onClick={()=> {props.logout(); localStorage.removeItem('user')}}>
                                <Link to="/">Logout</Link>
                            </div>
                        :
                        <React.Fragment>	
                            <Link to="/login">Login</Link>
                            <Link to="/create">Register</Link>
                        </React.Fragment>	
                        }
                    </div>

                

                </div>

                <div className = "bodyProfileContainer">
                    <div>
                        <AddNew/>
                    </div>
                    
                    <div className = "taskDisplayContainer clearfix">
                        <div>
                            <PendingTask/>
                        </div>

                        <div>
                            <DoneTask/>
                        </div>
                    </div>
                </div>
            </div>
        )
}

const mapStateToProps = state => {
	return {
		isLoggedIn: state.user.isLoggedIn
	}
}

const mapDispatchToProps = dispatch => {
	return {
		logout: () => dispatch({
			type: 'LOGOUT',
		})
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);